
# REST version of Spring Startapium Sample Application

This rest version of the Spring Startapium application only provides a REST API. There is no UI.
The [startapium-ui](https://bitbucket.org/*) is a front-end application witch consumes the REST API.

## Running startapium locally
```
	git clone https://bitbucket.org/startapium/startapium-rest.git
        cd startapium-rest
        mvn spring-boot:run
```

You can then access startapium here: http://localhost:8087/startapium/

## Swagger REST API documentation presented here:
http://localhost:8087/startapium/swagger-ui.html

## Database configuration

A similar setups is provided for MySQL in case a persistent database configuration is needed.
To run startapium locally using persistent database, it is needed to change profile defined in application.properties file.

For MySQL database, it is needed to set param "mysql" in string
```
spring.profiles.active=mysql,spring-data-jpa
```
 defined in application.properties file.

Before do this, would be good to check properties defined in application-mysql.properties file.

```
spring.datasource.url=jdbc:mysql://localhost:3306/startapium?autoReconnect=true&amp;useSSL=false
spring.datasource.driverClassName=com.mysql.jdbc.Driver
spring.datasource.username=root
spring.datasource.password=pass
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database=MYSQL
spring.jpa.database-platform=org.hibernate.dialect.MySQLDialect
spring.jpa.hibernate.ddl-auto=none
```

Files for create and populate the database: startapium_rest\src\main\resources\db\mysql
For all username: tester[number] , password: tester. 
For username: proselyte , password: proselyte. 
Only proselyte have got role --admin--.