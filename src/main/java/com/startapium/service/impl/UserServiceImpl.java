package com.startapium.service.impl;

import com.startapium.dao.RoleDAO;
import com.startapium.dao.UserDAO;
import com.startapium.model.Role;
import com.startapium.model.User;
import com.startapium.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Implementation of {@link UserService }interface
 *
 * @author Eugen Suleimanov
 * @version 1.0
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private RoleDAO roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.findByName("ROLE_USER"));
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    @Transactional
    public void update(User user) {
        userDao.save(user);
    }

    @Override
    @Transactional
    public User getById(UUID id){
        User user = userDao.getById(id);
        return user;
    }

    @Override
    @Transactional
    public User findByName(String username) {
        return userDao.findByName(username);
    }

    @Override
    @Transactional
    public Collection<User> getAll() {
        return userDao.getAll();
    }

    @Override
    @Transactional
    public void remove(User user) {
        this.userDao.remove(user);
    }

    @Override
    @Transactional
    public void remove(UUID id) {
        this.userDao.remove(id);
    }

    @Override
    public boolean coincidencePassword(CharSequence rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword,encodedPassword);
    }

}
