package com.startapium.service.impl;

import com.startapium.dao.RoleDAO;
import com.startapium.model.Role;
import com.startapium.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.UUID;

/**
 * Implementation of {@link RoleService }interface
 *
 * @author Eugen Suleimanov
 * @version 1.0
 */

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDao;

    @Override
    @Transactional
    public Role getById(UUID id) {
        return roleDao.getById(id);
    }

    @Override
    @Transactional
    public Role findByName(String name) {
        return roleDao.findByName(name);
    }

    @Override
    @Transactional
    public Collection<Role> getAll() {
        return roleDao.getAll();
    }

    @Override
    @Transactional
    public void save(Role role) {
        roleDao.save(role);
    }

    @Override
    @Transactional
    public void remove(Role role) {
        roleDao.remove(role);
    }

    @Override
    @Transactional
    public void remove(UUID id) {
        roleDao.remove(id);
    }
}
