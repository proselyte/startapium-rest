package com.startapium.service;


/**
 * service for security
 *
 * @author Eugen Suleimanov
 * @version 1.0
 */

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
