package com.startapium.service;

import com.startapium.model.Role;

/**
 * service class for {@link Role}
 *
 * @author Eugen Suleimanov
 * @version 1.0
 */
public interface RoleService extends BaseService<Role> {

}
