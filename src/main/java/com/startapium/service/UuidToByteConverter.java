package com.startapium.service;



import org.springframework.stereotype.Service;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.nio.ByteBuffer;
import java.util.UUID;

@Converter
@Service
public class UuidToByteConverter implements AttributeConverter<byte[],UUID> {


    @Override
    public UUID convertToDatabaseColumn(byte[] bytes) {

        if(bytes == null){
            return null;
        }
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        Long high = byteBuffer.getLong();
        Long low = byteBuffer.getLong();

        return new UUID(high, low);
    }

    @Override
    public byte[] convertToEntityAttribute(UUID uuid) {

        if(uuid == null){
            return null;
        }

        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());

        return bb.array();
    }
}

