package com.startapium.service;

import com.startapium.model.User;

/**
 * service class for {@link User}
 *
 * @author Eugen Suleimanov
 * @version 1.0
 */

public interface UserService extends BaseService<User> {

    void update(User user);

    boolean coincidencePassword(CharSequence rawPassword, String encodedPassword);
}
