package com.startapium.service;

import com.startapium.model.BaseEntity;

import java.util.Collection;
import java.util.UUID;

/**
 * Generic Service interface. Used as a base interface for all Service interfaces.
 *
 * @author Eugene Suleimanov
 */

public interface BaseService<T extends BaseEntity>{

    T getById(UUID id);

    T findByName(String name);

    Collection<T> getAll();

    void save(T role);

    void remove(T role);

    void remove(UUID id);
}
