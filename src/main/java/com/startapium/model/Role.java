package com.startapium.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.startapium.rest.RoleJacksonCustomDeserializer;
import com.startapium.rest.RoleJacksonCustomSerializer;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents Role of the {@link User}
 *
 * @author Eugene Suleimanov
 */

@Entity
@Table(name = "roles")
@JsonSerialize(using = RoleJacksonCustomSerializer.class)
@JsonDeserialize(using = RoleJacksonCustomDeserializer.class)
public class Role extends NamedEntity {

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @JsonIgnore
    public Set<User> getSetUsers() {
        return users;
    }

    public List<User> getUsers() {
        if(getSetUsers() != null){
            return new ArrayList<>(getSetUsers());
        }else{
            return null;
        }
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }


}
