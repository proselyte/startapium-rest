package com.startapium.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.startapium.rest.UserJacksonCustomDeserializer;
import com.startapium.rest.UserJacksonCustomSerializer;
import com.startapium.service.UuidToByteConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Simple JavaBean domain object that represents a User.
 *
 * @author Eugene Suleimanov
 */

@Entity
@Table(name = "users")
@JsonSerialize(using = UserJacksonCustomSerializer.class)
@JsonDeserialize(using = UserJacksonCustomDeserializer.class)
public class User extends NamedEntity {

    @Column(name = "email")
    private String email;

    @Column(name = "usertype")
    private String userType;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Transient
    private String confirmPassword;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date")
    private Date registrationDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "avatar_id")
    @Convert(converter = UuidToByteConverter.class)
    private UUID avatar_id;

    @ManyToMany
    @JoinTable(name = "user_roles", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private Set<Role> roles;
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "user_skills", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "skill_id", referencedColumnName = "id")})
//    private Set<Skill> skills;
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "user_specialties", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "specialty_id", referencedColumnName = "id")})
//    private Set<Specialty> specialties;
//
//    @OneToMany(mappedBy = "creator")
//    private Set<Team> createdTeams;
//
//    @OneToMany(mappedBy = "creatorProject")
//    private Set<Project> createdProjects;
//
//    @ManyToMany(mappedBy = "users")
//    private Set<Team> memberTeams;
//
//    @ManyToMany(mappedBy = "administrators")
//    private Set<Team> administratedTeams;
//
//    @ManyToMany(mappedBy = "requestedToJoinUsers")
//    private Set<Team> requestToJoinTeams;
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "vacancy_responded", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "vacancy_id", referencedColumnName = "id")})
//    private Set<Vacancy> respondedToPosition;
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "vacancy_watched", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "vacancy_id", referencedColumnName = "id")})
//    private Set<Vacancy> watched;




    @PrePersist
    public void getDate() {
        registrationDate = new Date();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @JsonIgnore
    public Set<Role> getSetRoles() {
        return roles;
    }

    public List<Role> getRoles() {
        if(getSetRoles() != null){
            return new ArrayList<>(getSetRoles());
        }else{
            return null;
        }
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public UUID getAvatar_id() {
        return avatar_id;
    }

    public void setAvatar_id(UUID avatar_id) {
        this.avatar_id = avatar_id;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", userType='" + userType + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", registrationDate=" + registrationDate +
                ", birthDate=" + birthDate +
                ", avatar=" + avatar_id +
                ", roles=" + roles +
                '}';
    }
}
