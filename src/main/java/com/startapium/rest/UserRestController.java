package com.startapium.rest;

import com.startapium.model.User;
import com.startapium.security.JwtTokenUtil;
import com.startapium.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * @author Evgene Suleimanov
 */

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("/app/user")
public class UserRestController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<User> getById(@PathVariable(name = "id") UUID id,
                                    UriComponentsBuilder ucBuilder) {
        HttpHeaders headers = new HttpHeaders();
        User user = userService.getById(id);
        if (user == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
//        headers.setLocation(ucBuilder.path("/pageUser/{userId}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<User>(user, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<User> getByName(@PathVariable(name = "name") String name,
                                    UriComponentsBuilder ucBuilder) {
        HttpHeaders headers = new HttpHeaders();
        User user = userService.findByName(name);
        if (user == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
//        headers.setLocation(ucBuilder.path("/pageUser/{userId}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<User>(user, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/empty/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> isEmptyUserName(@PathVariable(name = "name") String name,
                                          UriComponentsBuilder ucBuilder) {
        HttpHeaders headers = new HttpHeaders();
        User user = userService.findByName(name);
        if (user == null) {
            return new ResponseEntity<Void>(headers,HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Void>(headers,HttpStatus.OK);
    }
    @RequestMapping(value = "/all", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Map<String,List<User>>> listUsers(UriComponentsBuilder ucBuilder) {
        HttpHeaders headers = new HttpHeaders();
        List<User> users = new ArrayList<>(this.userService.getAll());
        List<User> customers = new ArrayList<>();
        List<User> specialists = new ArrayList<>();

        if (users.isEmpty()) {
            return new ResponseEntity<Map<String,List<User>>>(HttpStatus.NOT_FOUND);
        }

        for (User user:users) {
            if(user.getUserType().equals("customer")){
                customers.add(user);
            }else if(user.getUserType().equals("specialist")){
                specialists.add(user);
            }
        }
        Map<String,List<User>> usersMap = new HashMap<>();
        usersMap.put("users", users);
        usersMap.put("customers", customers);
        usersMap.put("specialists", specialists);

//        headers.setLocation(ucBuilder.path("/api/user/all").buildAndExpand().toUri());
        return new ResponseEntity<Map<String,List<User>>>(usersMap, HttpStatus.OK);

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<User> add(@RequestBody @Valid User user, BindingResult bindingResult,
                                        UriComponentsBuilder ucBuilder) {
        BindingErrorsResponse errors = new BindingErrorsResponse();
        HttpHeaders headers = new HttpHeaders();
        if (bindingResult.hasErrors() || (user == null)) {
            errors.addAllErrors(bindingResult);
            headers.add("errors", errors.toJSON());
            return new ResponseEntity<User>(headers, HttpStatus.BAD_REQUEST);
        }
        this.userService.save(user);
        user = this.userService.findByName(user.getName());
//        headers.setLocation(ucBuilder.path("/userPage/{userId}").buildAndExpand(user.getId()).toUri());

        return new ResponseEntity<User>(user, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<User> update(@RequestBody @Valid User user,BindingResult bindingResult,
                                       UriComponentsBuilder ucBuilder) {
        User userFromBase = this.userService.getById(user.getId());
        userFromBase.setFirstName(user.getFirstName());
        userFromBase.setLastName(user.getLastName());
        userFromBase.setName(user.getName());
        userFromBase.setUserType(user.getUserType());
        userFromBase.setEmail(user.getEmail());

        BindingErrorsResponse errors = new BindingErrorsResponse();
        HttpHeaders headers = new HttpHeaders();
        if (bindingResult.hasErrors()) {
            errors.addAllErrors(bindingResult);
            headers.add("errors", errors.toJSON());
            return new ResponseEntity<User>(headers, HttpStatus.BAD_REQUEST);
        }

        this.userService.update(userFromBase);
        user = this.userService.getById(user.getId());
//        headers.setLocation(ucBuilder.path("/userPage/{userId}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<User>(user, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> delete(@PathVariable("id") UUID id) {
        User user = this.userService.getById(id);
        if (user == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        this.userService.remove(user);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}