package com.startapium.rest;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.startapium.model.Role;

import java.io.IOException;
import java.util.UUID;

/**
 * Deserializer for entity Role extends class StdDeserializer
 *
 * @author Evgene Suleimanov
 */
public class RoleJacksonCustomDeserializer extends StdDeserializer<Role> {

    public RoleJacksonCustomDeserializer() {
        this(null);
    }

    public RoleJacksonCustomDeserializer(Class<Role> t) {
        super(t);
    }

    @Override
    public Role deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = parser.getCodec().readTree(parser);
        Role role = new Role();

        if (node.get("id").isNull()) {
            role.setId(null);
        } else {
            role.setId(UUID.fromString(node.get("id").asText()));
        }

        role.setName(node.get("name").asText(null));

        return role;
    }
}


