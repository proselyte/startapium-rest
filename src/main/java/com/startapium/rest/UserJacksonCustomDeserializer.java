package com.startapium.rest;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.startapium.model.Role;
import com.startapium.model.User;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Deserializer for entity User extends class StdDeserializer
 * @author Evgene Suleimanov
 */
public class UserJacksonCustomDeserializer extends StdDeserializer<User> {

    public UserJacksonCustomDeserializer(){
        this(null);
    }

    public UserJacksonCustomDeserializer(Class<User> t) {
        super(t);
    }

    @Override
    public User deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = parser.getCodec().readTree(parser);
        User user = new User();
        Set<Role> roles = new HashSet<>();
        if(node.has("id")){
            if(node.get("id").isNull()) {
                user.setId(null);
            }else {
                user.setId(UUID.fromString(node.get("id").asText()));
            }
        }

        user.setName(node.get("name").asText(null));
        user.setFirstName(node.get("firstName").asText(null));
        user.setLastName(node.get("lastName").asText(null));
        user.setEmail(node.get("email").asText(null));
        user.setUserType(node.get("userType").asText(null));
        user.setPassword(node.get("password").asText(null));

        DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");

        try {
            user.setBirthDate(formatter.parse(node.get("birthDate").asText(null)));
        } catch (ParseException e) {
            user.setBirthDate(null);
        }
        if(node.has("avatar_id")){
            if(node.get("avatar_id").isNull()) {
                user.setAvatar_id(null);
            }else {
                user.setAvatar_id(UUID.fromString(node.get("avatar_id").asText()));
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        List<JsonNode> role_nodes = node.findValues("role");
        for (JsonNode jsonNode : role_nodes){
            roles.add(mapper.treeToValue(jsonNode, Role.class));
        }
        user.setRoles(roles);

        return user;
    }

}
