package com.startapium.rest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.startapium.model.Role;
import com.startapium.model.User;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * Serializer for entity User extends class StdDeserializer
 * @author Evgene Suleimanov
 */

public class UserJacksonCustomSerializer extends StdSerializer<User> {



    public UserJacksonCustomSerializer() {
        this(null);
    }

    public UserJacksonCustomSerializer(Class<User> t) {
        super(t);
    }

    @Override
    public void serialize(User user, JsonGenerator jsonGen, SerializerProvider provider) throws IOException {

        Format formatter = new SimpleDateFormat("yyyy/MM/dd");

        jsonGen.writeStartObject();

        if (user.getId() == null) {
            jsonGen.writeNullField("id");
        } else {
            jsonGen.writeStringField("id", user.getId().toString());
        }

        jsonGen.writeStringField("name", user.getName());
        jsonGen.writeStringField("firstName", user.getFirstName());
        jsonGen.writeStringField("lastName", user.getLastName());
        jsonGen.writeStringField("email", user.getEmail());
        jsonGen.writeStringField("userType", user.getUserType());
        jsonGen.writeStringField("password", user.getPassword());
        jsonGen.writeStringField("registrationDate", formatter.format(user.getRegistrationDate()));
        if (user.getBirthDate() == null) {
            jsonGen.writeNullField("birthDate");
        } else {
            jsonGen.writeStringField("birthDate", formatter.format(user.getBirthDate()));
        }
        if (user.getAvatar_id() == null) {
            jsonGen.writeNullField("avatar_id");
        } else {
            jsonGen.writeStringField("avatar_id", user.getAvatar_id().toString());
        }


        jsonGen.writeArrayFieldStart("roles");
        for (Role role : user.getRoles()) {
            jsonGen.writeStartObject();
            if (role.getId() == null) {
                jsonGen.writeNullField("id");
            } else {
                jsonGen.writeStringField("id", role.getId().toString());
            }
            jsonGen.writeStringField("name", role.getName());
            jsonGen.writeEndObject();
        }
        jsonGen.writeEndArray();
        jsonGen.writeEndObject();
    }

}