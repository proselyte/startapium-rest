package com.startapium.rest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.startapium.model.Role;
import com.startapium.model.User;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * Serializer for entity Role extends class StdDeserializer
 * @author Evgene Suleimanov
 */
public class RoleJacksonCustomSerializer extends StdSerializer<Role> {



    public RoleJacksonCustomSerializer() {
        this(null);
    }

    public RoleJacksonCustomSerializer(Class<Role> t) {
        super(t);
    }

    @Override
    public void serialize(Role role, JsonGenerator jsonGen, SerializerProvider provider) throws IOException {

        jsonGen.writeStartObject();

        if (role.getId() == null) {
            jsonGen.writeNullField("id");
        } else {
            jsonGen.writeStringField("id", role.getId().toString());
        }

        jsonGen.writeStringField("name", role.getName());

        Format formatter = new SimpleDateFormat("yyyy/MM/dd");
        jsonGen.writeArrayFieldStart("users");

        if (role.getUsers() != null) {
            for (User user : role.getUsers()) {

                jsonGen.writeStartObject();

                if (user.getId() == null) {
                    jsonGen.writeNullField("id");
                } else {
                    jsonGen.writeStringField("id", user.getId().toString());
                }

                jsonGen.writeStringField("name", user.getName());
                jsonGen.writeStringField("firstName", user.getFirstName());
                jsonGen.writeStringField("lastName", user.getLastName());
                jsonGen.writeStringField("email", user.getEmail());
                jsonGen.writeStringField("userType", user.getUserType());
                jsonGen.writeStringField("password", user.getPassword());
                jsonGen.writeStringField("registrationDate", formatter.format(user.getRegistrationDate()));
                if (user.getBirthDate() == null) {
                    jsonGen.writeNullField("birthDate");
                } else {
                    jsonGen.writeStringField("birthDate", formatter.format(user.getBirthDate()));
                }
                if (user.getAvatar_id() == null) {
                    jsonGen.writeNullField("avatar_id");
                } else {
                    jsonGen.writeStringField("avatar_id", user.getAvatar_id().toString());
                }

                jsonGen.writeEndObject();
            }
        }

        jsonGen.writeEndArray();
        jsonGen.writeEndObject();
    }

}
