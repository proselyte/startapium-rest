package com.startapium.rest;

import com.startapium.model.Role;
import com.startapium.service.RoleService;
import com.startapium.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Evgene Suleimanov
 */

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("/admin/role")
public class RoleRestController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Role> pageRole(@PathVariable(name = "id") UUID id,
                                         UriComponentsBuilder ucBuilder) {
        HttpHeaders headers = new HttpHeaders();

        Role role = roleService.getById(id);
        if (role == null) {
            return new ResponseEntity<Role>(HttpStatus.NOT_FOUND);
        }
        headers.setLocation(ucBuilder.path("/pageRole/{id}").buildAndExpand(role.getId()).toUri());
        return new ResponseEntity<Role>(role, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Collection<Role>> listRoles(UriComponentsBuilder ucBuilder) {

        HttpHeaders headers = new HttpHeaders();
        Collection<Role> roles = this.roleService.getAll();
        if (roles.isEmpty()) {
            return new ResponseEntity<Collection<Role>>(HttpStatus.NOT_FOUND);
        }
        headers.setLocation(ucBuilder.path("/listRoles").buildAndExpand().toUri());
        return new ResponseEntity<Collection<Role>>(roles, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Role> addRole(@RequestBody @Valid Role role, BindingResult bindingResult,
                                        UriComponentsBuilder ucBuilder) {
        BindingErrorsResponse errors = new BindingErrorsResponse();
        HttpHeaders headers = new HttpHeaders();
        if (bindingResult.hasErrors() || (role == null)) {
            errors.addAllErrors(bindingResult);
            headers.add("errors", errors.toJSON());
            return new ResponseEntity<Role>(headers, HttpStatus.BAD_REQUEST);
        }
        this.roleService.save(role);
        role = this.roleService.findByName(role.getName());
        headers.setLocation(ucBuilder.path("/pageRole/{roleId}").buildAndExpand(role.getId()).toUri());
        return new ResponseEntity<Role>(role, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Role> update(@PathVariable("id") UUID id, @RequestBody @Valid Role role,
                                           BindingResult bindingResult, UriComponentsBuilder ucBuilder) {
        BindingErrorsResponse errors = new BindingErrorsResponse();
        HttpHeaders headers = new HttpHeaders();
        if (bindingResult.hasErrors() || (role == null)) {
            errors.addAllErrors(bindingResult);
            headers.add("errors", errors.toJSON());
            return new ResponseEntity<Role>(headers, HttpStatus.BAD_REQUEST);
        }
        roleService.save(role);
        role = this.roleService.getById(id);
        headers.setLocation(ucBuilder.path("/pageRole/{roleId}").buildAndExpand(role.getId()).toUri());
        ResponseEntity<Role> dffd = new ResponseEntity<Role>(role,headers, HttpStatus.CREATED);
        return dffd;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> delete(@PathVariable("id") UUID id) {
        Role role = this.roleService.getById(id);
        if (role == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        this.roleService.remove(role);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}