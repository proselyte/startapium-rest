package com.startapium.dao;

import com.startapium.model.BaseEntity;

/**
 * UniqueNameDAO DAO interface. Extends the interface BaseDAO and
 * add new method for search by unique name.
 *
 * @author Eugene Suleimanov
 */
public interface UniqueNameDAO<T extends BaseEntity, ID> extends BaseDAO<T, ID> {
    T findByName(String name);
}
