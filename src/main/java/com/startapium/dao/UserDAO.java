package com.startapium.dao;

import com.startapium.model.User;

import java.util.UUID;

/**
 * Extension of {@link BaseDAO} interface for class {@link User}.
 *
 * @author Eugene Suleimanov
 */
public interface UserDAO extends UniqueNameDAO<User, UUID> {
}
