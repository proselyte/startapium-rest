package com.startapium.dao;

import com.startapium.model.BaseEntity;

import java.util.Collection;

/**
 * Extends BaseDAO interface and
 * to add a new method to search by name.
 *
 * @author Eugene Suleimanov
 */

public interface NotUniqueNameDAO<T extends BaseEntity, ID> extends BaseDAO<T, ID> {
    Collection<T> findByName(String name);
}