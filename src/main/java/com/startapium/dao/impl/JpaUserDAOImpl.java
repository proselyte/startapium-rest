package com.startapium.dao.impl;

import com.startapium.dao.UserDAO;
import com.startapium.model.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.UUID;

/**
 * JPA implmentation of {@link UserDAO} interface.
 *
 * @author Eugene Suleimanov
 */

@Repository
public class JpaUserDAOImpl implements UserDAO {


    @PersistenceContext
    private EntityManager entityManager;


    private final static Logger logger = Logger.getLogger(JpaUserDAOImpl.class);

    @Override
    public User getById(UUID id) {


        try {
            Query query = this.entityManager.createQuery(
                    "SELECT DISTINCT user FROM  User user LEFT JOIN FETCH user.roles WHERE user.id =:id", User.class);
            query.setParameter("id", id);
            User user = (User)query.getSingleResult();

            logger.info("User successfully loaded. User details: " + user);

            return user;
        } catch (NoResultException e) {
            logger.info("User not found.ID = " + id);
            return null;
        }
    }


    @Override
    public User findByName(String username) {

        try {
            Query query = this.entityManager.createQuery(
                    "SELECT DISTINCT user FROM  User user LEFT JOIN FETCH user.roles WHERE user.name=:name", User.class);
            query.setParameter("name", username);
            User user = (User) query.getSingleResult();

            logger.info("User successfully found. User details: " + user);

            return user;
        } catch (NoResultException e) {
            logger.info("User not found.Username = " + username);
            return null;
        }
    }

    @Override
    public Collection<User> getAll() {

        Query query = this.entityManager.
                createQuery("SELECT DISTINCT user FROM User user");
        Collection<User> result = query.getResultList();

        for (User user : result) {
            logger.info("User list: " + user);
        }

        return result;
    }

    @Override
    public void save(User user) {

        if (user.getId() == null) {
            this.entityManager.persist(user);
            logger.info("User successfully saved. User details: " + user);
        } else {
            this.entityManager.merge(user);
            logger.info("User successfully updated. User details: " + user);
        }
    }

    @Override
    public void remove(User user) {
        this.remove(user.getId());
        logger.info("User successfully removed. User details: " + user);
    }

    @Override
    public void remove(UUID id) {

        this.entityManager.remove(this.entityManager.getReference(User.class, id));
    }

}