package com.startapium.dao;

import com.startapium.model.BaseEntity;

import java.util.Collection;

/**
 * Base DAO interface. Used as a base interface for all DAO classes.
 *
 * @author Eugene Suleimanov
 */
public interface BaseDAO<T extends BaseEntity, ID> {

    T getById(ID id);

    Collection<T> getAll();

    void save(T entity);

    void remove(T entity);

    void remove(ID id);
}