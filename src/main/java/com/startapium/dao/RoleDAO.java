package com.startapium.dao;

import com.startapium.model.Role;

import java.util.UUID;

/**
 * Extension of {@link BaseDAO} interface for class {@link Role}.
 *
 * @author Eugene Suleimanov
 */
public interface RoleDAO extends UniqueNameDAO<Role, UUID> {

}
