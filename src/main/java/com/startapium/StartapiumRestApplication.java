package com.startapium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.annotation.IntegrationComponentScan;

@SpringBootApplication
@IntegrationComponentScan
public class StartapiumRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartapiumRestApplication.class, args);
	}
}
