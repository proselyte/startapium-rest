

-- users
INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT , 'proselytear@yahoo.com', 'specialist', 'proselyte', 'Eugene', 'Suleimanov',
   '$2a$06$ZahiqJcoaZhVdCrSPMQp7OMbCkbrNZDn1hoiFvq4mDgUusmVo3PFm', '2016-12-01 12:00:00', '1988-10-28 12:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'tester1@yahoo.com', 'specialist', 'tester1', 'Tester1', 'Tester1',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'tester2@yahoo.com', 'specialist', 'tester2', 'Tester2', 'Tester2',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'tester3@yahoo.com', 'specialist', 'tester3', 'Tester3', 'Tester3',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'tester4@yahoo.com', 'specialist', 'tester4', 'Tester4', 'Tester4',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'tester5@yahoo.com', 'specialist', 'tester5', 'Tester5', 'Tester5',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'tester6@yahoo.com', 'specialist', 'tester6', 'Tester6', 'Tester6',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  (DEFAULT, 'requestUser1@yahoo.com', 'specialist', 'requestUser1', 'RequestUser1', 'RequestUser1',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

INSERT INTO users (id, email, usertype, name, first_name, last_name, password, registration_date, birth_date, avatar_id)
VALUES
  ('7a6891e1-ffdc-49da-9731-4971a4526214', 'requestUser2@yahoo.com', 'specialist', 'requestUser2', 'RequestUser2', 'RequestUser2',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00', NULL);

--roles
INSERT INTO roles(id,name) VALUES (RANDOM_UUID(), 'ROLE_USER');
INSERT INTO roles(id,name) VALUES (RANDOM_UUID(), 'ROLE_ADMIN');


-- user_roles
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='proselyte'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='proselyte'), (SELECT id FROM roles WHERE roles.name='ROLE_ADMIN'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='tester1'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='tester2'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='tester3'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='tester4'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='tester5'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='tester6'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='requestUser1'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));
INSERT INTO user_roles VALUES ((SELECT id FROM users WHERE users.name='requestUser2'), (SELECT id FROM roles WHERE roles.name='ROLE_USER'));