DELIMITER ;;
CREATE TRIGGER before_insert_files
BEFORE INSERT ON files
FOR EACH ROW
  BEGIN
    IF new.id IS NULL THEN
      SET new.id = UNHEX(REPLACE(UUID(), '-', ''));
    END IF;
  END
;;

DELIMITER ;;

CREATE TRIGGER before_insert_users
BEFORE INSERT ON users
FOR EACH ROW
  BEGIN
    IF new.id IS NULL THEN
      SET new.id = UNHEX(REPLACE(UUID(), '-', ''));
    END IF;
  END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_roles
BEFORE INSERT ON roles
FOR EACH ROW
  BEGIN
    IF new.id IS NULL THEN
      SET new.id = UNHEX(REPLACE(UUID(), '-', ''));
    END IF;
  END
;;