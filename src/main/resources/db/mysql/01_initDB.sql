-- users
CREATE TABLE IF NOT EXISTS users (
  id                BINARY(16)  NOT NULL,
  email             VARCHAR(255) NOT NULL,
  usertype          VARCHAR(50)  NOT NULL,
  name              VARCHAR(50)  NOT NULL,
  first_name        VARCHAR(50)  NOT NULL,
  last_name         VARCHAR(50)  NOT NULL,
  password          VARCHAR(255) NOT NULL,
  registration_date TIMESTAMP    NOT NULL,
  birth_date        TIMESTAMP,
  avatar_id         BINARY(16),

  FOREIGN KEY (avatar_id) REFERENCES files (id),
  PRIMARY KEY (id),
  UNIQUE(name)
)
  ENGINE = InnoDB;;

-- roles
CREATE TABLE IF NOT EXISTS roles (
  id   BINARY(16) NOT NULL,
  name VARCHAR(50) NOT NULL,

  UNIQUE(name),
  PRIMARY KEY (id)
)
ENGINE = InnoDB;

-- user_roles
CREATE TABLE IF NOT EXISTS user_roles (
  user_id BINARY(16) NOT NULL,
  role_id BINARY(16) NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (role_id) REFERENCES roles (id)

);