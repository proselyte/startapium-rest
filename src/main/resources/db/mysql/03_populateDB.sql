-- users
INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('proselytear@yahoo.com', 'specialist', 'proselyte', 'Eugene', 'Suleimanov',
   '$2a$06$ZahiqJcoaZhVdCrSPMQp7OMbCkbrNZDn1hoiFvq4mDgUusmVo3PFm', '2016-12-01 12:00:00', '1988-10-28 12:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('tester1@yahoo.com', 'specialist', 'tester1', 'Tester1', 'Tester1',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('tester2@yahoo.com', 'specialist', 'tester2', 'Tester2', 'Tester2',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('tester3@yahoo.com', 'specialist', 'tester3', 'Tester3', 'Tester3',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('tester4@yahoo.com', 'specialist', 'tester4', 'Tester4', 'Tester4',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('tester5@yahoo.com', 'specialist', 'tester5', 'Tester5', 'Tester5',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('tester6@yahoo.com', 'specialist', 'tester6', 'Tester6', 'Tester6',
   '$2a$06$W1wG8UVnrElk9u6xRjtx7uh5S56suavsALrrKWFIt5Ec/qPy3Z1qa', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

INSERT INTO users (email, usertype, name, first_name, last_name, password, registration_date, birth_date)
VALUES
  ('efficeon2011@yahoo.com', 'specialist', 'efficeon_admin', 'Leonid', 'Dubravsky',
   '$2a$10$jkFFFaac.Z2vdtYeVLShuO8AdSC5D1a3nRqysusK2A56L1Vv4.TRm', '2016-12-01 12:00:00', '1978-04-16 15:00:00');

-- roles
INSERT INTO roles(name) VALUES ('ROLE_USER');
INSERT INTO roles(name) VALUES ('ROLE_ADMIN');

-- user_roles
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='proselyte'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='proselyte'),
        (SELECT id FROM roles WHERE name='ROLE_ADMIN'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='tester1'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='tester2'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='tester3'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='tester4'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='tester5'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='tester6'),
        (SELECT id FROM roles WHERE name='ROLE_USER'));
INSERT INTO user_roles(user_id,role_id)
VALUES ((SELECT id FROM users WHERE name='efficeon_admin'),
        (SELECT id FROM roles WHERE name='ROLE_ADMIN'));